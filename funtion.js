const addNums = num1 => num1 + 5;

console.log(addNums(5));

function Person(firstname, lastname, dob){
    this.firstname = firstname;
    this.lastname = lastname;
    this.dob = new Date(dob);
    
}

Person.prototype.getBirthYear = function(){
    return this.dob.getFullYear();
}

Person.prototype.getFullName = function (){
    return `${this.firstname} ${this.lastname}`;
}
//instantiate object 

const person1 = new Person('John',' Doe', '4-3-1992');
const person2 = new Person('Samuel',' Lucifer', '4-3-1332');

//class
class Person{
    constructor(firstname, lastname, dob){
        this.firstname = firstname;
        this.lastname = lastname;
        this.dob = new Date(dob);
    }

    getBirthYear(){
        return this.dob.getFullYear();
    }
    getFullName(){
        return `${this.firstname} ${this.lastname}`;
    }
}
console.log(person2);
console.log(person1);
